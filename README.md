# Composite Data structures implementation for Go

author: Dipankar Das

> **Note**
Generics are used

```bash
go get gitlab.com/dipankardas011/composite_ds@<check the latest>
```
# Set in go

> **Note**
Need to update docs

```go
import ("gitlab.com/dipankardas011/composite_ds/set")

func main() {
	var set1 *set.Set[string] = &set.Set[string]{}
	set1.Create()
	set1.Add("a")
	set1.Add("b")
	set1.Add("aa")
	set1.Add("ab")

	fmt.Println("Set1")
	set1.PrintSet()

	var set2 *set.Set[string] = &set.Set[string]{}
	set2.Create()
	set2.Add("3")
	set2.Add("b")

	fmt.Println("Set2")
	set2.PrintSet()

	setU_DC := *set1.UnionDeepCopy(*set2)
	fmt.Println("Union deepcopy")
	setU_DC.PrintSet()

	setI_DC := *set1.IntersectionDeepCopy(*set2)
	fmt.Println("Intersection deepcopy")
	setI_DC.PrintSet()

	set1.Union(set2)
	fmt.Println("Union")
	set1.PrintSet()

	// as set1 is modified in line above so it is
	// intersection with update one
	set1.Intersection(set2)
	fmt.Println("Intersection")
	set1.PrintSet()
}
```

# LRU cache in go

```go
package main

import (
	"fmt"

	"gitlab.com/dipankardas011/composite_ds/lru"
)

func main() {
	var lruCache lru.Lru[string, int]
	lruCache.Init()

	fmt.Println("Cache for 'dipankar' -> ", lruCache.Get("dipankar"))

	lruCache.Update("brave_1", 121)
	fmt.Println("Cache for 'brave_1' -> ", lruCache.Get("brave_1"))
	lruCache.Update("brave_2", 122)
	lruCache.Update("brave_3", 123)
	lruCache.Update("brave_4", 124)
	lruCache.Update("brave_5", 125)
	lruCache.Update("brave_7", 127)
	lruCache.Update("chrome_12", 212)
	lruCache.Update("brave_10", 130)
	lruCache.Update("brave_11", 131)
	lruCache.Update("brave_21", 141)
	lruCache.Update("brave_22", 142)

	fmt.Println("Cache for 'brave_1' -> ", lruCache.Get("brave_1"))
	fmt.Println("Cache for 'brave_4' -> ", lruCache.Get("brave_4"))
}

```
