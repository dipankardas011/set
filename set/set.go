package set

import (
	"fmt"
)

type Set[K comparable] struct {
	data map[K]bool
}

// allocates the memory to the Set
func (m *Set[K]) Create() {
	m.data = make(map[K]bool, 0)
}

// adds elements of comparable type <Generics>
func (m *Set[K]) Add(key K) {
	m.data[key] = true
}

// union operations and creates a seperate set object
func (set1 Set[K]) UnionDeepCopy(set2 Set[K]) *Set[K] {
	var union *Set[K] = &Set[K]{}
	union.Create()

	for k, _ := range set1.data {
		union.data[k] = true
	}

	for k, _ := range set2.data {
		union.data[k] = true
	}
	return union
}

// union which modifies the calling object
func (set1 *Set[K]) Union(set2 *Set[K]) {

	for k, _ := range set2.data {
		set1.data[k] = true
	}
}

// intersetion which modifies the calling object
func (set1 *Set[K]) Intersection(set2 *Set[K]) {
	if len(set1.data) < len(set2.data) {
		set1, set2 = set2, set1
	}

	for k, _ := range set1.data {
		if set2.data[k] {
			set1.data[k] = true
		} else {
			set1.data[k] = false
		}
	}
}

// Intersection which creates a seperate set object
func (set1 Set[K]) IntersectionDeepCopy(set2 Set[K]) *Set[K] {
	var intersect *Set[K] = &Set[K]{}
	intersect.Create()

	if len(set1.data) < len(set2.data) {
		set1, set2 = set2, set1
	}

	for k, _ := range set1.data {
		if set2.data[k] {
			intersect.data[k] = true
		}
	}
	return intersect
}

func (set *Set[K]) PrintSet() {
	out := `[ `
	for k, d := range set.data {
		if d {
			out += fmt.Sprintf(" %v", k)
		}
	}
	out += ` ]`
	fmt.Println(out)
}
