import java.util.HashMap;

class Node<T> {
    public T value;
    public Node<T> next;
    public Node<T> prev;

    public Node(T value) {
        this.value = value;
        this.next = null;
        this.prev = null;
    }
}

public class Lru<K, V> {
    public int length;
    private int capacity;
    private Node<V> head;
    private Node<V> tail;

    private HashMap<K,Node<V>> lookup;
    private HashMap<Node<V>, K> reverseLookup;

    public Lru() {
        this.length = 0;
        this.capacity = 10;
        this.head = null;
        this.tail = null;
        this.lookup = new HashMap<>();
        this.reverseLookup = new HashMap<>();
    }

    public void Update(K key, V value) {
        Node<V> node = this.lookup.get(key);

        if (node == null) {
            this.length++;
            Node<V> nodeNew = new Node<>(value);
            this.attach(nodeNew);
            this.trimCache();

            this.lookup.put(key, nodeNew);
            this.reverseLookup.put(nodeNew, key);
        } else {
            this.detach(node);
            node.value = value;
            this.attach(node);
        }
    }

    private void trimCache() {
        if (this.length <= this.capacity) {
            return ;
        }
        Node<V> tail = this.tail;
        this.detach(tail);
        K key = this.reverseLookup.get(tail);
        this.lookup.remove(key);
        this.reverseLookup.remove(tail);
        this.length--;
    }

    public V Get(K key) {
        Node<V> node = this.lookup.get(key);

        if (node == null) {
            return null;
        }

        this.detach(node);
        this.attach(node);

        return node.value;
    }

    private void attach(Node<V> node) {
        if (this.head == null) {
            this.head = this.tail = node;
            return;
        }
        node.next = this.head;
        this.head.prev = node;
        this.head = node;
    }

    private void detach(Node<V> node) {
        if (node.prev != null) {
            node.prev.next = node.next;
        }
        if (node.next != null) {
            node.next.prev = node.prev;
        }

        if (this.length == 1) {
            this.head = this.tail = null;
        }

        if (this.head == node) {
            this.head = this.head.next;
        }
        if (this.tail == node) {
            this.tail = this.tail.prev;
        }

        node.prev = null;
        node.next = null;

    }

    public static void main(String[] args) {
        Lru<String, Integer> lru_cache = new Lru<>();
        lru_cache.Update("brave_1", 121);
        System.out.println("Get of 'brave_1' -> "+lru_cache.Get("brave_1"));

        lru_cache.Update("brave_2", 122);
        lru_cache.Update("brave_3", 123);
        lru_cache.Update("brave_4", 124);
        lru_cache.Update("brave_5", 126);

        lru_cache.Update("brave_6", 128);
        lru_cache.Update("brave_7", 129);
        lru_cache.Update("chrome_3", 1203);
        lru_cache.Update("chrome_4", 1204);
        lru_cache.Update("chrome_9", 1209);
        lru_cache.Update("chrome_10", 1210);

        System.out.println("Get of '1' -> "+lru_cache.Get("1"));
        System.out.println("Get of 'brave_1' -> "+lru_cache.Get("brave_1"));
        System.out.println("Get of 'chrome_10' -> "+lru_cache.Get("chrome_10"));

    }
}
