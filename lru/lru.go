package lru

type Node[T comparable] struct {
	value T
	next  *Node[T]
	prev  *Node[T]
}

const (
	capacity = 10
)

type Lru[K, V comparable] struct {
	length int
	head   *Node[V]
	tail   *Node[V]

	lookup        map[K]*Node[V]
	reverseLookup map[*Node[V]]K
}

func createNode[V comparable](value V) *Node[V] {
	return &Node[V]{value: value, next: nil, prev: nil}
}

func (lru *Lru[K, V]) Init() {
	lru.length = 0
	lru.head = nil
	lru.tail = nil

	lru.lookup = make(map[K]*Node[V])
	lru.reverseLookup = make(map[*Node[V]]K)
}

func (lru *Lru[K, V]) Update(key K, value V) {
	var node *Node[V] = lru.lookup[key]

	if node == nil {
		lru.length++
		var nodeNew *Node[V] = createNode(value)
		lru.attach(nodeNew)
		lru.trimCache()

		lru.lookup[key] = nodeNew
		lru.reverseLookup[nodeNew] = key
	} else {
		lru.detach(node)
		node.value = value
		lru.attach(node)
	}
}

func (lru *Lru[K, V]) Get(key K) V {

	var node *Node[V] = lru.lookup[key]

	if node == nil {
		var ret V
		return ret
	}

	lru.detach(node)
	lru.attach(node)

	return node.value
}

func (lru *Lru[K, V]) attach(node *Node[V]) {
	if lru.head == nil {
		lru.head = node
		lru.tail = node
		return
	}

	node.next = lru.head
	lru.head.prev = node
	lru.head = node
}

func (lru *Lru[K, V]) detach(node *Node[V]) {
	if node.prev != nil {
		node.prev.next = node.next
	}
	if node.next != nil {
		node.next.prev = node.prev
	}

	if lru.head == node {
		lru.head = lru.head.next
	}
	if lru.tail == node {
		lru.tail = lru.tail.prev
	}

	node.prev = nil
	node.next = nil
}

func (lru *Lru[K, V]) trimCache() {
	if lru.length <= capacity {
		return
	}

	var tail *Node[V] = lru.tail
	lru.detach(tail)
	var key K = lru.reverseLookup[tail]

	delete(lru.lookup, key)
	delete(lru.reverseLookup, tail)

	lru.length--
}
